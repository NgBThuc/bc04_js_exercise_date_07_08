let numberArray = [];

// Display result
function displayResult(selector, content) {
  document.querySelector(selector).textContent = content;
}

// Add number button
function addNumber() {
  let inputNumber = document.querySelector("#inputNumber").value;
  if (inputNumber !== "") {
    numberArray.push(+inputNumber);
    document.querySelector("#inputNumber").value = "";
    document.querySelector("#inputNumber").focus();
    displayResult("#arrayCurrent", numberArray);
    displayResult("#countInt__currentArray", numberArray);
  }
}

// Delete number button
function deleteNumber() {
  numberArray.pop();
  displayResult("#arrayCurrent", numberArray);
  displayResult("#countInt__currentArray", numberArray);
}

// Exercise 1 - Total of positive number
function sumPositiveNum() {
  let sum = 0;
  numberArray.forEach(function (number) {
    if (number > 0) {
      sum += number;
    }
  });
  displayResult("#arrayTotal", sum);
}

// Exercise 2 - Count positive number
function countPositiveNum() {
  let count = 0;
  numberArray.forEach(function (number) {
    if (number > 0) {
      count++;
    }
  });
  displayResult("#arrayCount", count);
}

// Exercise 3 - Find smallest number
function findSmallestNum() {
  displayResult("#arraySmallestNum", Math.min(...numberArray));
}

// Exercise 4 - Find smallest positive number
function findSmallestPositiveNum() {
  let filteredArray = numberArray.filter(function (number) {
    return number > 0;
  });
  displayResult("#arraySmallestPositiveNum", Math.min(...filteredArray));
}

// Exercise 5 - Find last even number
function findLastEvenNum() {
  let evenArray = numberArray.filter(function (number) {
    return number % 2 === 0;
  });
  displayResult(
    "#arrayLastEvenNumber",
    evenArray.length === 0 ? -1 : evenArray[evenArray.length - 1]
  );
}

// Exercise 6 - Swap any two values in an array
function swapNum() {
  let position1 = document.querySelector("#swap__position1").value * 1;
  let position2 = document.querySelector("#swap__position2").value * 1;
  [numberArray[position1], numberArray[position2]] = [
    numberArray[position2],
    numberArray[position1],
  ];
  displayResult("#arraySwap", numberArray);
  displayResult("#arrayCurrent", numberArray);
}

// Exercise 7 - Sort ascending
function sortAscending() {
  let sortedArray = numberArray.sort((a, b) => {
    return a - b;
  });
  displayResult("#arraySort", sortedArray);
}

// Exercise 8 - Find the first prime number
function isPrime(number) {
  if (number < 2) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }
  return true;
}

function findFirstPrime() {
  for (let i = 0; i < numberArray.length; i++) {
    if (isPrime(numberArray[i])) {
      displayResult("#arrayFirstPrime", numberArray[i]);
      return;
    }
  }
  displayResult("#arrayFirstPrime", -1);
}

// Exercise 9 - Count integers
function addRealNum() {
  let realNum = document.querySelector("#countInt__realNum").value;

  if (realNum !== "") {
    numberArray.push(+realNum);
    document.querySelector("#countInt__realNum").value = "";
    document.querySelector("#countInt__realNum").focus();
    displayResult("#arrayCurrent", numberArray);
    displayResult("#countInt__currentArray", numberArray);
  }
}

function countInt() {
  let countInt = 0;
  numberArray.forEach(function (number) {
    if (Number.isInteger(number)) {
      countInt++;
    }
  });
  displayResult("#arrayCountInt", countInt);
}

// Exercise 10 - Compare the number of negative and positive number

function compareNum() {
  let negativeCount = 0;
  let positiveCount = 0;
  numberArray.forEach(function (number) {
    if (number < 0) {
      negativeCount++;
    } else if (number > 0) {
      positiveCount++;
    }
  });
  if (negativeCount > positiveCount) {
    displayResult(
      "#arrayCompare",
      `Negative number (${negativeCount}) > Positive number (${positiveCount})`
    );
  } else if (negativeCount < positiveCount) {
    displayResult(
      "#arrayCompare",
      `Negative number (${negativeCount}) < Positive number (${positiveCount})`
    );
  } else {
    displayResult(
      "#arrayCompare",
      `Negative number (${negativeCount}) = Positive number (${positiveCount})`
    );
  }
}
